import java.sql.*;

public class ConnexionBD{
    
    private ConnexionMySQL laconnexion;

    public ConnexionBD(ConnexionMySQL laconnexion){
        this.laconnexion = laconnexion;
    }

    public String connexion(String pseudo, String mdp) throws SQLException{
        Statement st = this.laconnexion.createStatement();
        ResultSet rs = st.executeQuery("select nomRole from ROLE natural join UTILISATEUR where pseudoUt = " + pseudo + " and mdpUt = " + mdp);
        if (rs.next()){
            if (rs.getString(1).charAt(0) == 'J'){
                throw new SQLException("Le module joueur n'est pas implémenté.");
            }
            return rs.getString(1);
        }
        else{
            throw new SQLException("Cet utilisateur n'existe pas !");
        }
    }

    public boolean creation(String pseudo, String mdp, String mail, byte[] avatar) throws SQLException{
        PreparedStatement pst = this.laconnexion.prepareStatement("insert into UTILISATEUR values(?,?,?,?,?,?,?)");
        Statement st = this.laconnexion.createStatement();
        ResultSet rs = st.executeQuery("select IFNULL(max(idUt),0) lemax from UTILISATEUR");
        rs.next();
        int res = rs.getInt("lemax");
        rs.close();
        pst.setInt(1, res+1);
        pst.setString(2, pseudo);
        pst.setString(3, mail);
        pst.setString(4, mdp);
        pst.setString(5, "N");
        Blob b = this.laconnexion.createBlob();
        b.setBytes(1, avatar);
        pst.setBlob(6, b);
        pst.setInt(7, 2);
        pst.executeUpdate();
        pst.executeUpdate();
        return true;
    }

}