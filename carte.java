public class carte{
 
    private String carte; 
    private boolean etat;
    
    public carte(String carte, boolean etat){
        this.carte = carte;
        this.etat = etat;
    }

    public String getCarte(){
        return this.carte;
    }

    public boolean getEtat(){
        return this.etat;
    }
}
