import java.sql.*;

public class ConnexionMySQL {

    private Connection mysql;
    private boolean connecte = false;

    /** Charge le driver */
    public ConnexionMySQL() throws ClassNotFoundException{
        try{
            Class.forName("org.mariadb.jdbc.Driver");
        }
        catch(ClassNotFoundException e){
            System.out.println("Driver MySQL non trouvé");
            mysql = null;
            return;
        }
    }

    /** crée la connexion */
    public void connecter() throws SQLException {
        try{
            mysql = DriverManager.getConnection("jdbc:mysql://" + "46.105.92.223" + ":3306/" + "db31a","groupe31a","21@info!iuto31a");
            connecte = true;
        }
        catch(SQLException e){
            System.out.println("Echec de connection");
            System.out.println(e.getMessage());
            mysql = null;
            return;
        }
    }

    /** ferme la connexion */
    public void close() throws SQLException {

    }

    public boolean isConnecte(){
        return this.connecte;
    }

    public Blob createBlob()throws SQLException{
        return this.mysql.createBlob();
    }

    public Statement createStatement() throws SQLException {
        return this.mysql.createStatement();
    }

    public PreparedStatement prepareStatement(String requete) throws SQLException{
        return this.mysql.prepareStatement(requete);
    }
}
